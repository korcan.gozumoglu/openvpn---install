# OpenVPN - Install

Run script as

    $ bash create-vpn.sh
    

Edit "#server_ip_address" in "client-common-passive.txt" accordingly.

Copy "client-common-passive.txt" to ~/etc/openvpn/ and delete it from main directory if you want.

    $ cp client-common-passive.txt ~/etc/openvpn/
    $ rm client-common-passive.txt
    
    
Edited for personal use from https://github.com/Angristan/OpenVPN-install